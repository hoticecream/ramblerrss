package ru.hoticecream.ramblerrss.activities;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.hoticecream.ramblerrss.R;
import ru.hoticecream.ramblerrss.fragments.DetailsFragment;
import ru.hoticecream.ramblerrss.fragments.FeedsListFragment;


public class MainActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        if (savedInstanceState == null) {
            showFeedsList();
        }
    }

    private void showFeedsList() {
        FeedsListFragment fragment = FeedsListFragment.newInstance();
        getFragmentManager().beginTransaction()
                .add(R.id.frame, fragment)
                .commit();
    }

    public void showFeed(String title, String link) {
        Fragment currentFragment = getFragmentManager().findFragmentById(R.id.frame);
        DetailsFragment fragment = DetailsFragment.newInstance(title, link);
        getFragmentManager().beginTransaction()
                .add(R.id.frame, fragment)
                .addToBackStack("details")
                .hide(currentFragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
