package ru.hoticecream.ramblerrss;

import javax.inject.Singleton;

import dagger.Component;
import ru.hoticecream.ramblerrss.fragments.FeedsListFragment;
import ru.hoticecream.ramblerrss.services.GazetaLoadingJob;
import ru.hoticecream.ramblerrss.services.LentaLoadingJob;

@Singleton
@Component(modules = RssModule.class)
public interface RssComponent {
    void injectLentaJob(LentaLoadingJob lentaLoadingJob);

    void injectGazetaJob(GazetaLoadingJob gazetaLoadingJob);

    void injectFeedsListFragment(FeedsListFragment feedsListFragment);
}
