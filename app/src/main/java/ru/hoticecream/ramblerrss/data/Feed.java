package ru.hoticecream.ramblerrss.data;


public class Feed {
    private String link;
    private String title;
    private String description;
    private String guid;
    private long pubDate;
    private int source;
    private String image;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public long getPubDate() {
        return pubDate;
    }

    public void setPubDate(long pubDate) {
        this.pubDate = pubDate;
    }

    public int getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source.ordinal();
    }

    public String getSourceTitle() {
        return Source.values()[source].getTitle();
    }

    public void setSource(int source) {
        this.source = source;
    }

    public void setImageUrl(String image) {
        this.image = image;
    }

    public String getImageUrl() {
        return image;
    }

    public enum Source {
        LENTA("Lenta.ru"),
        GAZETA("Gazeta.ru");

        private final String title;

        Source(String title) {
            this.title = title;
        }

        public String getTitle() {
            return title;
        }
    }

}
