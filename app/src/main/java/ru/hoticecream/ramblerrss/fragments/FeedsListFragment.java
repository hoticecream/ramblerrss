package ru.hoticecream.ramblerrss.fragments;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.util.SortedList;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import ru.hoticecream.ramblerrss.R;
import ru.hoticecream.ramblerrss.RamblerRss;
import ru.hoticecream.ramblerrss.activities.MainActivity;
import ru.hoticecream.ramblerrss.data.Feed;
import ru.hoticecream.ramblerrss.services.RssLoadingManager;
import ru.hoticecream.ramblerrss.storage.FeedRecord;
import ru.hoticecream.ramblerrss.storage.FeedsDao;
import ru.hoticecream.ramblerrss.utils.FeedsConverter;


public class FeedsListFragment extends Fragment
        implements LoaderManager.LoaderCallbacks<List<FeedsListFragment.FeedItem>>,
        SwipeRefreshLayout.OnRefreshListener, RssLoadingManager.IStatusListener {

    private static final int FEEDS_LOADER_ID = 1;
    private FeedsAdapter mAdapter;
    private Realm mRealm;
    private RealmChangeListener mRealmListener = new RealmChangeListener() {
        @Override
        public void onChange() {
            getLoaderManager().restartLoader(FEEDS_LOADER_ID, null, FeedsListFragment.this);
        }
    };

    public static FeedsListFragment newInstance() {
        return new FeedsListFragment();
    }


    @Inject
    RssLoadingManager mRssManager;
    @Inject
    FeedsDao mFeedsDao;

    @Bind(R.id.recyclerview)
    RecyclerView mRecyclerView;
    @Bind(R.id.refresh_layout)
    SwipeRefreshLayout mRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RamblerRss.getRssModule().injectFeedsListFragment(this);
        mRssManager.addStatusListener(this);
        mRealm = mFeedsDao.setListener(mRealmListener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feeds_list, container, false);
        ButterKnife.bind(this, view);
        mRefreshLayout.setOnRefreshListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new FeedsAdapter() {
            @Override
            public void onFeedSelected(FeedItem currentItem) {
                ((MainActivity) getActivity()).showFeed(currentItem.getTitle(), currentItem.getLink());
            }
        };
        mRecyclerView.setAdapter(mAdapter);
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);
            }
        });

        onRefresh();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getLoaderManager().initLoader(FEEDS_LOADER_ID, null, this);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            getActivity().setTitle(R.string.main_title);
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    @Override
    public void onDestroy() {
        mRssManager.removeStatusListener(this);
        mRealm.removeAllChangeListeners();
        mRealm.close();
        super.onDestroy();
    }

    @Override
    public Loader<List<FeedItem>> onCreateLoader(int i, Bundle bundle) {
        return new FeedsLoader(getActivity(), mFeedsDao);
    }

    @Override
    public void onLoadFinished(Loader<List<FeedItem>> loader, List<FeedItem> feedItems) {
        mAdapter.add(feedItems);
    }

    @Override
    public void onLoaderReset(Loader<List<FeedItem>> loader) {
        mAdapter.clear();
    }

    @Override
    public void onRefresh() {
        mRssManager.refreshFeeds();
    }

    @Override
    public void onSuccess() {
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onFailure(Exception e) {
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(false);
            }
        });
        Toast.makeText(getActivity(), "Error loading feeds: " + (e == null ? null : e.getLocalizedMessage()),
                Toast.LENGTH_SHORT).show();
    }

    public static class FeedsLoader extends AsyncTaskLoader<List<FeedItem>> {

        private final FeedsDao mFeedsDao;

        public FeedsLoader(Context context, FeedsDao dao) {
            super(context);
            mFeedsDao = dao;
        }

        @Override
        public List<FeedItem> loadInBackground() {
            List<FeedRecord> records = mFeedsDao.list();
            List<FeedItem> result = new ArrayList<>(records.size());
            for (FeedRecord r : records) {
                result.add(new FeedItem(FeedsConverter.convertToFeed(r)));
            }
            return result;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }
    }

    private static abstract class FeedsAdapter extends RecyclerView.Adapter<FeedHolder> {

        private SortedList.BatchedCallback<FeedItem> mCallback = new SortedList.BatchedCallback<>(
                new SortedListAdapterCallback<FeedItem>(this) {
                    @Override
                    public int compare(FeedItem o1, FeedItem o2) {
                        return o2.compareTo(o1);
                    }

                    @Override
                    public boolean areContentsTheSame(FeedItem oldItem, FeedItem newItem) {
                        return oldItem.equals(newItem);
                    }

                    @Override
                    public boolean areItemsTheSame(FeedItem item1, FeedItem item2) {
                        return TextUtils.equals(item1.getGuid(), item2.getGuid());
                    }
                });

        private SortedList<FeedItem> mItems = new SortedList<>(FeedItem.class, mCallback);

        public FeedsAdapter() {
            setHasStableIds(true);
        }


        @Override
        public long getItemId(int position) {
            return mItems.get(position).getGuid().hashCode();
        }

        @Override
        public FeedHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_feed, parent, false);
            return new FeedHolder(view) {

                @Override
                public void onOpen() {
                    onFeedSelected(getCurrentItem());
                }
            };
        }

        public abstract void onFeedSelected(FeedItem currentItem);

        @Override
        public void onBindViewHolder(FeedHolder holder, int position) {
            holder.bind(mItems.get(position));
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void clear() {
            mItems.clear();
        }

        public void add(List<FeedItem> feedItems) {
            mItems.addAll(feedItems);
            mCallback.dispatchLastEvent();
        }
    }

    static abstract class FeedHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private static final DateFormat DATE_FORMAT = DateFormat.getDateTimeInstance();
        @Bind(R.id.text_title)
        TextView mTextTitle;
        @Bind(R.id.text_date)
        TextView mTextDate;
        @Bind(R.id.text_description)
        TextView mTextDescription;
        @Bind(R.id.text_source)
        TextView mTextSource;
        @Bind(R.id.image_photo)
        ImageView mImagePhoto;


        private FeedItem mCurrentItem;

        public FeedHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void bind(FeedItem item) {
            mCurrentItem = item;
            mTextTitle.setText(item.getTitle());
            mTextDate.setText(DATE_FORMAT.format(new Date(item.getPubDate())));
            mTextDescription.setText(item.getDescription());
            mTextDescription.setVisibility(View.GONE);
            mTextSource.setText(item.getSourceTitle());
            if (TextUtils.isEmpty(item.getImageUrl())) {
                mImagePhoto.setVisibility(View.GONE);
            } else {
                mImagePhoto.setVisibility(View.VISIBLE);
                Glide.with(mImagePhoto.getContext()).load(item.getImageUrl()).into(mImagePhoto);
            }

        }

        public FeedItem getCurrentItem() {
            return mCurrentItem;
        }

        @Override
        public void onClick(View v) {
            if (mTextDescription.getVisibility() == View.VISIBLE) {
                mTextDescription.setVisibility(View.GONE);
            } else {
                mTextDescription.setVisibility(View.VISIBLE);
            }
        }

        @OnClick(R.id.button_open)
        public abstract void onOpen();
    }

    public static class FeedItem implements Comparable<FeedItem> {

        private final Feed record;


        public FeedItem(Feed feed) {
            this.record = feed;
        }

        public String getLink() {
            return record.getLink();
        }

        public String getTitle() {
            return record.getTitle();
        }

        public String getDescription() {
            return record.getDescription();
        }

        public String getGuid() {
            return record.getGuid();
        }

        public long getPubDate() {
            return record.getPubDate();
        }

        public String getSourceTitle() {
            return record.getSourceTitle();
        }

        public String getImageUrl() {
            return record.getImageUrl();
        }

        @Override
        public int compareTo(FeedItem item) {
            return (int) (getPubDate() - item.getPubDate());
        }

        @Override
        public int hashCode() {
            return super.hashCode(); // TODO
        }

        @Override
        public boolean equals(Object o) {
            return o != null
                    && o instanceof FeedItem
                    && TextUtils.equals(getGuid(), ((FeedItem) o).getGuid());
        }
    }
}
