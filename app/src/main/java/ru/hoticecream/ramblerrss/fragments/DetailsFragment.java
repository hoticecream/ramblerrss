package ru.hoticecream.ramblerrss.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ru.hoticecream.ramblerrss.R;
import ru.hoticecream.ramblerrss.activities.MainActivity;


public class DetailsFragment extends Fragment {

    private static final String KEY_TITLE = "key_title";
    private static final String KEY_LINK = "key_link";
    private WebView mWebView;
    private String mTitle;
    private String mLink;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle = getArguments().getString(KEY_TITLE);
        mLink = getArguments().getString(KEY_LINK);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        mWebView = (WebView) view;
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.loadUrl(mLink);
        //ButterKnife.bind(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(mTitle);
        ((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public static DetailsFragment newInstance(String title, String link) {
        Bundle bundle = new Bundle();
        bundle.putString(KEY_TITLE, title);
        bundle.putString(KEY_LINK, link);
        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }
}
