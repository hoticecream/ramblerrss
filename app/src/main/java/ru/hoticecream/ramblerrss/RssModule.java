package ru.hoticecream.ramblerrss;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.hoticecream.ramblerrss.services.RssLoadingManager;
import ru.hoticecream.ramblerrss.storage.FeedsDao;
import ru.hoticecream.ramblerrss.utils.GazetaFeedsLoader;
import ru.hoticecream.ramblerrss.utils.LentaFeedsLoader;

@Module
public class RssModule {

    private Context mContext;

    public RssModule(Context context) {
        mContext = context;
    }

    @Provides
    @Singleton
    public FeedsDao provideFeedsDao() {
        return new FeedsDao(mContext);
    }

    @Provides
    @Singleton
    public LentaFeedsLoader provideLentaFeedsLoader() {
        return new LentaFeedsLoader();
    }

    @Provides
    @Singleton
    public GazetaFeedsLoader provideGazetaFeedsLoader() {
        return new GazetaFeedsLoader();
    }

    @Provides
    @Singleton
    public RssLoadingManager provideFeedsLoaderManager() {
        return new RssLoadingManager(mContext);
    }

}
