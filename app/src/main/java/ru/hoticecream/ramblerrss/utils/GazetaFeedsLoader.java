package ru.hoticecream.ramblerrss.utils;


import org.xmlpull.v1.XmlPullParserException;

public class GazetaFeedsLoader extends FeedsLoader {
    @Override
    public String getUrl() {
        return "http://www.gazeta.ru/export/rss/lenta.xml";
    }

    @Override
    public IParser getParser() throws XmlPullParserException {
        return new Rss2Parser();
    }
}
