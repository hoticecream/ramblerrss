package ru.hoticecream.ramblerrss.utils;


import android.util.Log;

public class Logger {
    private static final String TAG = "RamblerRSS";

    public static void d(String message) {
        Log.d(TAG, message);
    }
}
