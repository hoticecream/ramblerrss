package ru.hoticecream.ramblerrss.utils;


import org.xmlpull.v1.XmlPullParserException;

public class LentaFeedsLoader extends FeedsLoader {
    @Override
    public String getUrl() {
        return "http://lenta.ru/rss";
    }

    @Override
    public IParser getParser() throws XmlPullParserException {
        return new Rss2Parser();
    }
}
