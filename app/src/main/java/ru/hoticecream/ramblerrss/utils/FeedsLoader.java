package ru.hoticecream.ramblerrss.utils;


import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.List;

import ru.hoticecream.ramblerrss.data.Feed;

public abstract class FeedsLoader {

    private static final OkHttpClient sOkClient = new OkHttpClient();

    public OkHttpClient getClient() {
        return sOkClient;
    }

    public List<Feed> loadFeeds() throws IOException, ParseException, XmlPullParserException {
        Request request = new Request.Builder()
                .url(getUrl())
                .build();
        Response response = getClient().newCall(request).execute();
        return getParser().parse(response.body().byteStream());
    }

    public abstract String getUrl();

    public abstract IParser getParser() throws XmlPullParserException;


    public static interface IParser {
        List<Feed> parse(InputStream stream) throws IOException, XmlPullParserException, ParseException;
    }
}
