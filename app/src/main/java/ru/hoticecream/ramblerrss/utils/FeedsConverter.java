package ru.hoticecream.ramblerrss.utils;


import ru.hoticecream.ramblerrss.data.Feed;
import ru.hoticecream.ramblerrss.storage.FeedRecord;

public class FeedsConverter {


    public static FeedRecord convertToFeedRecord(Feed f, Feed.Source source) {
        FeedRecord record = new FeedRecord();
        record.setGuid(f.getGuid());
        record.setTitle(f.getTitle());
        record.setDescription(f.getDescription());
        record.setLink(f.getLink());
        record.setPubDate(f.getPubDate());
        record.setSource(source.ordinal());
        record.setImage(f.getImageUrl());
        return record;
    }

    public static Feed convertToFeed(FeedRecord record) {
        Feed feed = new Feed();
        feed.setGuid(record.getGuid());
        feed.setTitle(record.getTitle());
        feed.setDescription(record.getDescription());
        feed.setLink(record.getLink());
        feed.setPubDate(record.getPubDate());
        feed.setSource(record.getSource());
        feed.setImageUrl(record.getImage());
        return feed;
    }
}
