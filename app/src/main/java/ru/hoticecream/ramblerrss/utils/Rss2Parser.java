package ru.hoticecream.ramblerrss.utils;

import android.text.Html;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ru.hoticecream.ramblerrss.data.Feed;


public class Rss2Parser implements FeedsLoader.IParser {

    private final List<Feed> articleList = new ArrayList<Feed>();
    private static final SimpleDateFormat RFC822 = new SimpleDateFormat(
            "EEE, dd MMM yyyy HH:mm:ss Z", java.util.Locale.ENGLISH);
    private final XmlPullParser xmlParser;

    public Rss2Parser() throws XmlPullParserException {
        // Initialize XmlPullParser object with a common configuration
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(false);
        xmlParser = factory.newPullParser();
    }

    public List<Feed> parse(InputStream input) throws IOException, XmlPullParserException, ParseException {
        // Clear previous list and start timing execution time
        articleList.clear();
        xmlParser.setInput(input, null);

        // Reuse Article object and event holder
        Feed article = new Feed();
        int eventType = xmlParser.getEventType();

        // Loop through the entire xml feed
        while (eventType != XmlPullParser.END_DOCUMENT) {
            String tagname = xmlParser.getName();
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (tagname.equalsIgnoreCase("item")) { // Start a new instance
                        article = new Feed();
                    } else // Handle this node if not an entry tag
                        handleNode(tagname, article);
                    break;
                case XmlPullParser.END_TAG:
                    if (tagname.equalsIgnoreCase("item")) {
                        // Add article object to list
                        articleList.add(article);
                    }
                    break;
                default:
                    break;
            }
            eventType = xmlParser.next();
        }
        return articleList;
    }

    /**
     * Handles a node from the tag node and assigns it to the correct article value.
     *
     * @param tag     The tag which to handle.
     * @param article Article object to assign the node value to.
     * @return True if a proper tag was given or handled. False if improper tag was given or
     * if an exception if triggered.
     */
    private boolean handleNode(String tag, Feed article) throws IOException, XmlPullParserException, ParseException {
        int next = xmlParser.next();
        if (next != XmlPullParser.TEXT && next != XmlPullParser.END_TAG && next != XmlPullParser.START_TAG)
            return false;

        if (tag.equalsIgnoreCase("link"))
            article.setLink(xmlParser.getText());
        else if (tag.equalsIgnoreCase("title"))
            article.setTitle(xmlParser.getText());
        else if (tag.equalsIgnoreCase("description")) {
            String encoded = xmlParser.getText();
            article.setDescription(Html.fromHtml(encoded).toString());
        } else if (tag.equalsIgnoreCase("pubDate")) {
            article.setPubDate(getParsedDate(xmlParser.getText()));
        } else if (tag.equalsIgnoreCase("guid")) {
            article.setGuid(xmlParser.getText());
        } else if (tag.equalsIgnoreCase("category")) {
        } else if (tag.equalsIgnoreCase("author")) {
        } else if (tag.equalsIgnoreCase("enclosure")) {
            article.setImageUrl(xmlParser.getAttributeValue("", "url"));
        }

        return true;
    }

    /**
     * Converts a date in the "EEE, d MMM yyyy HH:mm:ss Z" format to a long value.
     *
     * @param encodedDate The encoded date which to convert.
     * @return A long value for the passed date String or 0 if improperly parsed.
     */
    protected long getParsedDate(String encodedDate) throws ParseException {
        return RFC822.parse(encodedDate).getTime();
    }
}
