package ru.hoticecream.ramblerrss;

import android.app.Application;


public class RamblerRss extends Application {

    private static RssComponent sRssModule;

    @Override
    public void onCreate() {
        super.onCreate();

        sRssModule = DaggerRssComponent.builder().rssModule(new RssModule(this)).build();
    }

    public static RssComponent getRssModule() {
        return sRssModule;
    }
}
