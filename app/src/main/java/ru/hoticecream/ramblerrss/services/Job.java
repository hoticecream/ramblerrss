package ru.hoticecream.ramblerrss.services;


import android.os.Parcelable;

public abstract class Job implements Parcelable {

    public abstract void execute() throws JobException;

    public static class JobException extends Exception {
        public JobException(Exception e) {
            super(e);
        }
    }
}
