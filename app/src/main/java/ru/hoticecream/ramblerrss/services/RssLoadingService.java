package ru.hoticecream.ramblerrss.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;


public class RssLoadingService extends IntentService {
    private static final String EXTRA_JOB = "job";
    private static final int RESULT_OK = 0;
    private static final int RESULT_FAILED = -1;
    static final String ACTION_JOB_RESULT = "ru.hoticecream.ramblerrss.JOB_RESULT";
    static final String EXTRA_JOB_RESULT = "job_result";
    static final String EXTRA_JOB_EXCEPTION = "job_exception";

    public RssLoadingService() {
        super("rss_loading");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Job job = intent.getParcelableExtra(EXTRA_JOB);
        try {
            job.execute();
            sendResult(RESULT_OK, null);
        } catch (Job.JobException e) {
            e.printStackTrace();
            sendResult(RESULT_FAILED, e);
        }
    }

    private void sendResult(int result, Exception e) {
        Intent intent = new Intent(ACTION_JOB_RESULT);
        intent.putExtra(EXTRA_JOB_RESULT, result);
        if (e != null) {
            intent.putExtra(EXTRA_JOB_EXCEPTION, e);
        }
        sendBroadcast(intent);
    }

    public static void post(Context context, Job job) {
        Intent intent = new Intent(context, RssLoadingService.class);
        intent.putExtra(EXTRA_JOB, job);
        context.startService(intent);
    }
}
