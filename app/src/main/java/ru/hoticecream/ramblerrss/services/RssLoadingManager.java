package ru.hoticecream.ramblerrss.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public class RssLoadingManager {

    private final Context mContext;
    private final List<WeakReference<IStatusListener>> mStatusListeners = new LinkedList<>();
    private final BroadcastReceiver mStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int result = intent.getIntExtra(RssLoadingService.EXTRA_JOB_RESULT, -1);
            if (result == 0) {
                onSuccess();
            } else {
                Exception e = (Exception) intent.getSerializableExtra(
                        RssLoadingService.EXTRA_JOB_EXCEPTION);
                onFailure(e);
            }
        }
    };


    public RssLoadingManager(Context context) {
        mContext = context;
        IntentFilter filter = new IntentFilter(RssLoadingService.ACTION_JOB_RESULT);
        mContext.registerReceiver(mStatusReceiver, filter);
    }

    public void refreshFeeds() {
        RssLoadingService.post(mContext, new LentaLoadingJob());
        RssLoadingService.post(mContext, new GazetaLoadingJob());
    }

    public void addStatusListener(IStatusListener listener) {
        mStatusListeners.add(new WeakReference<>(listener));
    }

    public void removeStatusListener(IStatusListener listener) {
        for (Iterator<WeakReference<IStatusListener>> iterator = mStatusListeners.iterator();
             iterator.hasNext(); ) {
            IStatusListener l = iterator.next().get();
            if (l == null) {
                iterator.remove();
                continue;
            }
            if (l == listener) {
                iterator.remove();
                return;
            }
        }
    }

    private void onSuccess() {
        for (Iterator<WeakReference<IStatusListener>> iterator = mStatusListeners.iterator();
             iterator.hasNext(); ) {
            IStatusListener l = iterator.next().get();
            if (l == null) {
                iterator.remove();
                continue;
            }
            l.onSuccess();
        }
    }

    private void onFailure(Exception e) {
        for (Iterator<WeakReference<IStatusListener>> iterator = mStatusListeners.iterator();
             iterator.hasNext(); ) {
            IStatusListener l = iterator.next().get();
            if (l == null) {
                iterator.remove();
                continue;
            }
            l.onFailure(e);
        }
    }

    // TODO add job id
    public interface IStatusListener {
        void onSuccess();

        void onFailure(Exception e);
    }
}
