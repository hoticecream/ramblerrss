package ru.hoticecream.ramblerrss.services;


import android.os.Parcel;

import javax.inject.Inject;

import ru.hoticecream.ramblerrss.RamblerRss;
import ru.hoticecream.ramblerrss.data.Feed;
import ru.hoticecream.ramblerrss.storage.FeedsDao;
import ru.hoticecream.ramblerrss.utils.FeedsLoader;
import ru.hoticecream.ramblerrss.utils.LentaFeedsLoader;

public class LentaLoadingJob extends RssLoadingJob {

    @Inject
    LentaFeedsLoader mLoader;

    @Inject
    FeedsDao mFeedsDao;

    public LentaLoadingJob() {
        RamblerRss.getRssModule().injectLentaJob(this);
    }

    public FeedsLoader getLoader() {
        return mLoader;
    }

    @Override
    protected Feed.Source getSource() {
        return Feed.Source.LENTA;
    }

    public FeedsDao getFeedsDao() {
        return mFeedsDao;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    protected LentaLoadingJob(Parcel in) {
        this();
    }

    public static final Creator<LentaLoadingJob> CREATOR = new Creator<LentaLoadingJob>() {
        public LentaLoadingJob createFromParcel(Parcel source) {
            return new LentaLoadingJob(source);
        }

        public LentaLoadingJob[] newArray(int size) {
            return new LentaLoadingJob[size];
        }
    };
}
