package ru.hoticecream.ramblerrss.services;

import java.util.ArrayList;
import java.util.List;

import ru.hoticecream.ramblerrss.data.Feed;
import ru.hoticecream.ramblerrss.storage.FeedRecord;
import ru.hoticecream.ramblerrss.storage.FeedsDao;
import ru.hoticecream.ramblerrss.utils.FeedsConverter;
import ru.hoticecream.ramblerrss.utils.FeedsLoader;


public abstract class RssLoadingJob extends Job {
    @Override
    public void execute() throws JobException {
        try {
            List<Feed> feeds = getLoader().loadFeeds();
            List<FeedRecord> records = new ArrayList<>();
            for (Feed f : feeds) {
                records.add(FeedsConverter.convertToFeedRecord(f, getSource()));
            }
            getFeedsDao().addAll(records);
        } catch (Exception e) {
            e.printStackTrace();
            throw new JobException(e);
        }
    }

    protected abstract FeedsDao getFeedsDao();

    protected abstract FeedsLoader getLoader();

    protected abstract Feed.Source getSource();
}
