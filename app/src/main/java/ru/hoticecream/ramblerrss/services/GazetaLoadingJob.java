package ru.hoticecream.ramblerrss.services;


import android.os.Parcel;

import javax.inject.Inject;

import ru.hoticecream.ramblerrss.RamblerRss;
import ru.hoticecream.ramblerrss.data.Feed;
import ru.hoticecream.ramblerrss.storage.FeedsDao;
import ru.hoticecream.ramblerrss.utils.FeedsLoader;
import ru.hoticecream.ramblerrss.utils.GazetaFeedsLoader;

public class GazetaLoadingJob extends RssLoadingJob {

    @Inject
    GazetaFeedsLoader mLoader;

    @Inject
    FeedsDao mFeedsDao;

    public GazetaLoadingJob() {
        RamblerRss.getRssModule().injectGazetaJob(this);
    }

    public FeedsLoader getLoader() {
        return mLoader;
    }

    @Override
    protected Feed.Source getSource() {
        return Feed.Source.GAZETA;
    }

    public FeedsDao getFeedsDao() {
        return mFeedsDao;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    protected GazetaLoadingJob(Parcel in) {
        this();
    }

    public static final Creator<GazetaLoadingJob> CREATOR = new Creator<GazetaLoadingJob>() {
        public GazetaLoadingJob createFromParcel(Parcel source) {
            return new GazetaLoadingJob(source);
        }

        public GazetaLoadingJob[] newArray(int size) {
            return new GazetaLoadingJob[size];
        }
    };
}
