package ru.hoticecream.ramblerrss;

import android.content.Context;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.MediumTest;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import ru.hoticecream.ramblerrss.storage.FeedsDao;

import static android.support.test.InstrumentationRegistry.getTargetContext;

@RunWith(AndroidJUnit4.class)
@MediumTest
public class DITest {

    @Inject
    FeedsDao mFeedsDao;

    @Before
    public void setUp() throws Exception {
        Context context = getTargetContext();
        MockRssComponent component = DaggerMockRssComponent.builder()
                .rssModule(new RssModule(context)).build();
        component.injectDITest(this);
    }

    @Test
    public void testDI() throws Exception {
        Assert.assertNotNull(mFeedsDao);
        Assert.assertNotNull(mFeedsDao.list());
    }
}
