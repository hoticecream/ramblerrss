package ru.hoticecream.ramblerrss.utils;

import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.MediumTest;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import ru.hoticecream.ramblerrss.data.Feed;

@RunWith(AndroidJUnit4.class)
@MediumTest
public class FeedsLoaderTest {

    @Test
    public void testLentaLoadFeeds() throws Exception {
        LentaFeedsLoader loader = new LentaFeedsLoader();
        loadFeeds(loader);
    }

    @Test
    public void testGazetaLoadFeeds() throws Exception {
        GazetaFeedsLoader loader = new GazetaFeedsLoader();
        loadFeeds(loader);
    }

    private void loadFeeds(FeedsLoader loader) throws ParseException, XmlPullParserException, IOException {
        List<Feed> feedList = loader.loadFeeds();
        Assert.assertTrue(feedList.size() != 0);
        Feed firstFeed = feedList.get(0);
        Assert.assertNotNull(firstFeed.getDescription());
        Assert.assertNotNull(firstFeed.getGuid());
        Assert.assertNotNull(firstFeed.getLink());
        Assert.assertTrue(firstFeed.getPubDate() != 0);
        Assert.assertNotNull(firstFeed.getTitle());
    }
}