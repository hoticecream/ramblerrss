package ru.hoticecream.ramblerrss;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Provides;

@Singleton
@Component(modules = RssModule.class)
public interface MockRssComponent extends RssComponent {

    void injectDITest(DITest target);
}
