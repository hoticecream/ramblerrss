package ru.hoticecream.ramblerrss.services;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static android.support.test.InstrumentationRegistry.getTargetContext;


public class RssLoadingManagerTest {

    private RssLoadingManager mLoadingManager;

    @Before
    public void setUp() throws Exception {
        Context context = getTargetContext();
        mLoadingManager = new RssLoadingManager(context);
    }


    private Exception mException;

    @Test
    public void testRefreshFeeds() throws Exception {
        final CountDownLatch latch = new CountDownLatch(2);
        mException = null;
        final RssLoadingManager.IStatusListener listener = new RssLoadingManager.IStatusListener() {
            @Override
            public void onSuccess() {
                latch.countDown();
            }

            @Override
            public void onFailure(Exception e) {
                mException = e;
                latch.countDown();

            }
        };
        mLoadingManager.addStatusListener(listener);
        mLoadingManager.refreshFeeds();
        if (!latch.await(100000, TimeUnit.MILLISECONDS)) {
            throw new Exception("Timeout for loading feeds");
        }
        if (mException != null) {
            throw mException;
        }
    }
}