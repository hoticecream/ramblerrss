package ru.hoticecream.ramblerrss.storage;


import android.content.Context;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;

public class FeedsDao {

    private static final String DB_NAME = "feeds";

    public FeedsDao(Context context) {
        RealmConfiguration config = new RealmConfiguration.Builder(context)
                .name(DB_NAME)
                .build();
        Realm.setDefaultConfiguration(config);
    }

    public List<FeedRecord> list() {
        Realm realm = Realm.getDefaultInstance();
        return realm.allObjects(FeedRecord.class); // TODO should close realm instance
    }

    public void addAll(List<FeedRecord> feeds) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(feeds);
        realm.commitTransaction();
        realm.close();
    }

    public Realm setListener(RealmChangeListener listener) {
        Realm realm = Realm.getDefaultInstance();
        realm.addChangeListener(listener);
        return realm;
    }
}
