package ru.hoticecream.ramblerrss.storage;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FeedRecord extends RealmObject {

    public static final String ID = "id";
    public static final String LINK = "link";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String GUID = "guid";
    public static final String PUB_DATE = "pubDate";
    public static final String SOURCE = "source";


    private String link;
    private String title;
    private String description;
    @PrimaryKey
    private String guid;
    private long pubDate;
    private int source;
    private String image;


    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public long getPubDate() {
        return pubDate;
    }

    public void setPubDate(long pubDate) {
        this.pubDate = pubDate;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }
}
