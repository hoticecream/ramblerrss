package ru.hoticecream.ramblerrss.storage;

import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.MediumTest;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static android.support.test.InstrumentationRegistry.getTargetContext;

@RunWith(AndroidJUnit4.class)
@MediumTest
public class FeedsDaoTest {


    private FeedsDao mFeedsDao;

    @Before
    public void setUp() throws Exception {
        mFeedsDao = new FeedsDao(getTargetContext());
    }

    @Test
    public void testAddAndList() throws Exception {
        List<FeedRecord> records = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            FeedRecord record = new FeedRecord();
            record.setGuid(Integer.toString(i));
            records.add(record);
        }
        mFeedsDao.addAll(records);
        List<FeedRecord> loaded = mFeedsDao.list();
        Assert.assertEquals(records.size(), loaded.size());
    }

}